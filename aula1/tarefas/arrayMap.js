//Cria lista de textos

var lista = [];

lista.push( "Hoje é sábado." );
lista.push( "O sol está quente." );
lista.push( "A água está quente." );
lista.push( "Mês que vem tem feriado." );
lista.push( "No verão vou viajar." );

function arrayMap( lista ){
    return lista.split( "" ).reverse().join( "" );
}
var retorno = lista.map( arrayMap );
console.log( retorno );
