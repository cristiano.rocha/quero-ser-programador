//Cria lista de textos

var lista = [];

lista.push( "Amanhã vou comprar um carro." );
lista.push( "Está nevando no Canadá." );
lista.push( "Quem fez o tema?" );
lista.push( "Antes tarde do que nunca." );
lista.push( "Você não pode fazer isso." );
lista.push( "Perdi minha carteira ontem." );

function arraySort( a, b ){
    return a.localeCompare( b );
}   
var retorno = lista.sort( arraySort );
console.log( retorno );