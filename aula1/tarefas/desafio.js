function removeItens( lista, remover ) {
    return lista.filter( function ( item ) {
        return !~ remover.indexOf( item );
    }
    );
}
var numeros = [1, 2, 3, 4, 5];
var remove1 = removeItens( numeros, [1, 2] );
var remove2 = removeItens( numeros, [1, 3] );
var remove3 = removeItens( numeros, [3, 4] );
console.log( remove1 );
console.log( remove2 );
console.log( remove3 );

