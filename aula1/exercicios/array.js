//Cria a lista
var lista = []

lista.push( 0 )
lista.push( 1 )
lista.push( 2 )
lista.push( 3 )
lista.push( 4 )
lista.push( 5 )
lista.push( 6 )
lista.push( 7 )
lista.push( 8 )
lista.push( 9 )
lista.push( 10 )


//Usando o comando Array.filter filtre os números menores que 5
//nova lista

function arrayFilter( lista ){
    return lista < 5;
}
var retorno = lista.filter( arrayFilter );
    console.log( retorno );


///Usando o comando Array.map multiplique a lista por 3
//nova lista

function arrayMap( lista ){
    return lista * 3;
}
var retorno = lista.map( arrayMap );
console.log ( retorno );


//Usando o comando Array.find localize o numero 5 na lista
//valor/elemento

function arrayFind( lista ){
    return lista === 5;
}
var retorno = lista.find( arrayFind );
console.log( retorno );