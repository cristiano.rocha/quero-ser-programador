type somaType = ( param1:number, param2:number )=>number;
let fun: somaType;

function somarDados( param1:number, param2:number ):number {
    return param1 + param2;
}
fun = somarDados;
console.log( fun(15, 20) );
console.log( fun(10, 50) );
console.log( fun(35, 14) );
