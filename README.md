##### **Por que eu seria um bom programador?**  
Bom, sempre gostei de tecnologias, poder criar algo novo, tirar projetos do papel.  
Hoje, estando em meio a uma empresa de desenvolvimento de software que tem adotado as melhores tecnologias e práticas de mercado, o que possibilita a correção e melhoria do sistema utilizando estas tecnologias não só otimizando os processos, mas tornando a interface mais amigável ao cliente final, o que faz com que o sistema torne-se cada vez mais eficiente, fidelizando o cliente, e assim aumentando as perspectivas de crescimento para todos envolvidos, cliente final alavancando suas vendas, empresa expandindo em território e em qualidade do produto que oferece e colaboradores crescendo profissionalmente, o que possibilita o desenvolvedor a ir atrás de novas tecnologias e aprimorar seus conhecimentos em prol de todo este processo mencionado.  
Isto me dá a possibilidade de poder contribuir para o time num todo. Aproveitar a oportunidade existente e poder me tornar um programador também.  
Pois vai fazer eu crescer como profissional e como pessoa, além de poder descobrir novas tecnologias, novas formas de contribuir no aprimoramento do sistema.  
Embora sempre tive gosto por tecnologias, a falta de internet em casa sempre foi um obstáculo para mim, o que reconheço que tem me atrasado nesta busca.
Claro, possuo acesso a internet na empresa, através de apostilas, livros, mas acredito que não seja a mesma coisa estudar e aprender do que estudar no sossego de casa estando conectado a internet onde poderei pesquisar mais exemplos, ter acesso a mais informações, entender novas tecnologias, e poder contribuir futuramente até com tarefas em hora extra, porque mais do que o valor recebido na hora extra, acredito que o conhecimento aprendido vale mais, pois terei que pesquisar o que não souber e no final, além da tarefa solicitada entregue, certamente terei agregado conhecimento em alguma nova funcionalidade específica, pois vejo isso acontecendo com colegas que nesta situação aprendem algo novo que pode ser utilizado não só em casa como no dia a dia na empresa.  
Agora, depois de anos, consegui encontrar uma `solução de internet` que irá me atender sem que eu precise me mudar, o que acredito que realmente irá alavancar meu conhecimento, pois terei **N** possibilidades de cursos, exemplos, estudos a praticar e poder transcrever para correções e melhorias no sistema da empresa.  
Enfim, sempre gostei de tecnologia como já mencionado, mas não tive todas ferramentas possíveis em mãos para praticar, mas agora estou adquirindo isto ao mesmo tempo que há a oportunidade para me tornar um programador.  

---  

**Abaixo algumas características de porque quero ser um bom programador:**  
- Adquirir novos conhecimentos em tecnologias;  
- Vontade de aprender;  
- Trabalhar em equipe;  
- Desenvolver um bom raciocínio lógico;  
- Fortalecer o inglês;
- Ser paciente e aprender a lidar com os erros existentes;  
- Gostar do que faz.  
 - **Obs:** Quando gostamos do que fazemos, tudo torna-se mais fácil.

---  

Explorando mais os recursos do [Markdonw](https://gitlab.com/help/user/markdown), qual seu parecer sobre meu **README.md**?  
1. [ ] Péssimo :cry: 
2. [ ] Ruim :slight\_frown: 
3. [ ] Regular  :slight\_smile:   
4. [ ] Bom  :grinning:  
5. [ ] Ótimo :heart: 